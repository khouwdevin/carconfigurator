// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomBlueprintFunctionLibrary.h"

FColor UCustomBlueprintFunctionLibrary::ChangeHextoColor(FString hex)
{
	FColor currentColor = FColor::FromHex(hex);
	return currentColor;
}
